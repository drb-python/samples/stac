import time
import schedule
import gen_stac


if __name__ == '__main__':
    print('MAIN')
    # perform preprocess once at start to populate service
    gen_stac.gen_stac()

    # perform periodically (once per day)
    schedule.every(1).days.do(gen_stac.gen_stac)
    while True:
        schedule.run_pending()
        try:
            time.sleep(1)
        except KeyboardInterrupt:
            exit(0)
