# DRB Stac Sample
The first objective of this script is to demonstrate how to use Drb to query Odata API and retrieve online products content to expose them as a stac streamline catalogue.

The SpatioTemporal Asset Catalog (STAC) family of specifications aim to standardize the way geospatial asset metadata is structured and queried. This project primary objective is to generate Stac JSON catalogue/collection/item structure using DRB.

## Quick Stac Item Sample
This stac module is able to generate stac items by direct extraction of the metadata from the online odata service. A quick demonstration of this capability with the following snippet:

```python
import json
import os
from requests.auth import HTTPBasicAuth
from drb_impl_odata import ODataServiceNode, ODataQueryPredicate
import drb_stac.odata_stac as odata
import logging

logger = logging.getLogger('QUICK-START')

# Initialize online service context
SERVICE = os.environ.get('gss_service')
USERNAME = os.environ.get('gss_username')
PASSWORD = os.environ.get('gss_password')

# Connect DRB to the service
auth = HTTPBasicAuth(USERNAME, PASSWORD)
node = ODataServiceNode(SERVICE, auth)

# Filters (OData) S2A datasets
s2a = ODataQueryPredicate(filter="startswith(Name,'S2A')", top=10)

# got generator among the results
products = node/s2a

count = 0
try:
    # Loop among generated item to be printed
    for item in odata.item_content_from_odata_nodes(products):
        logger.info(json.dumps(item, default=str, indent=3))
        count += 1
except Exception:
    logger.exception(f"Failed after {count} items created.")

logger.info(f"{count} Items created.")
```
Service connection is provided in this example via environment variables.

Results of this snippet shows the set of metadata extracted from OData in items stac form. This item only contains information regarding the item itself. Stac links with collection and catalogue are managed in the Model management (See next section):

```json
{
   "id": "S2A_OPER_MSI_L0__GR_SGS__20180420T195109_S20180420T163556_D06_N02.06.tar",
   "geometry": {
      "type": "Polygon",
      "coordinates": [
         [
            [
               -87.1046083578953,
               27.0342869996124
            ],
            [
               -87.0091398629774,
               27.3981345441031
            ],
            [
               -87.2587972912068,
               27.4487803251302
            ],
            [
               -87.3532717079262,
               27.0846594328354
            ],
            [
               -87.1046083578953,
               27.0342869996124
            ]
         ]
      ]
   },
   "bbox": [
      -87.3532717079262,
      -87.0091398629774,
      27.0342869996124,
      27.4487803251302
   ],
   "properties": {
      "created": "2021-12-21T19:34:11.587272",
      "updated": "2021-12-21T19:34:11.587272",
      "datetime": null,
      "start_datetime": "2018-04-20T16:35:56.000Z",
      "stop_datetime": "2018-04-20T16:35:56.000Z",
      "title": "Sentinel-2 Level-0 PDI Granule (short name: PDI L0 GR)",
      "description": "Level-0 Granule PDI is defined as a tar file. It consists of:\n\n                  1. Level-0_Granule_Metadata_File: XML metadata file containing the requested level of information and referring all the product elements composing the Granule;\n\n                  2. IMG_DATA: folder containing the mission data corresponding to one on-board scene for one detector and all spectral bands. The image data are provided as a set of 13 binary files, one for each spectral band, including all corresponding Image Source Packets (ISP) in the observation chronological sequence. The ISPs include their corresponding source packet annotations as a pre-pended header of each source packet;\n\n                  3. QI_DATA: folder containing XML reports about Geometric quality, Image content quality, Quality control checks information;\n\n                  4. Inventory_Metadata.xml: file containing the metadata needed to inventory the PDI;\n\n                  5. manifest.safe: XML SAFE Manifest file;\n\n                  6. rep_info: folder containing the available XSD schemas that validate the PDI components.\n\n                  Note that the Inventory_Metadata.xml, manifest.safe and rep_info are available inside a Granule but they are removed when the PDI is included in the User Product.",
      "eo:cloud_cover": 0.0,
      "eo:orbit": 14766
   },
}
...
```

To create a complete STAC Item, the following Item model is available:

```python
from drb_stac.stac_model import Item, Link, LinkRelEnum

link = link = [Link(rel=LinkRelEnum.SELF,
                    href="http://www.drb-python.org/sample/item")]
stac_item = Item(id=item['id'],
                 geometry=item['geometry'],
                 bbox=item['bbox'],
                 properties=item['properties'],
                 assets=item['assets'],
                 links=[link])
```
The link sample used here to setup at least 'self' link is managed in the 
layout part. This example cretes sample layout including only one self link.

`drb_stac.stac_model.Item` is a complete auto-validated structure of the standard stac item. It can be accessed as a single dic and its content is compliant with stac version 1.0.0. The validation is performed thanks to the json schemas (https://github.com/radiantearth/stac-spec/tree/v1.0.0/item-spec/json-schema) embedded in this project [resources/schema/item.json](resources/schema/item.json) to avoid unwanted internet connection while validation process.

 `drb_stac.stac_model.Collection` and `drb_stac.stac_model.Catalog` does not requires data from product source.
They shall be manually configured to prepare a complete stac catalogue. Unitary test [test_full_generation.py](tests/test_full_generation.py) shows how to handle the creation of catalogs and collections.

## Layout management
Thanks to the [Item](drb_stac/stac_model.py#L258), [Collection](drb_stac/stac_model.py#L209), [Catalog](drb_stac/stac_model.py#L171) classes in model, The SpatioTemporal Asset Catalog content is ready and can be write into a file system de be handled by a STAC API.

layout module aims to manage links between stack elements and implements the write feature to dans the complete organisation.

```python
from drb_stac.layout.stac_layout import StacLayout

layout_mgr = StacLayout(
    root_href="https://stac.gael-systems.com",
    root_title="STAC repository")

catalog_layout = layout_mgr.catalog_layout(
    identifier=catalog_id, title=catalog_title, collections=collections)

collection1_layout = layout_mgr.collection_layout(
    parent_id=catalog_id, parent_title=catalog_title,
    identifier=collection1_id, title=collection1_title)

catalog_layout.write(stac_catalog, working_dir='test_out_folder')
collection1_layout.write(collections[0], working_dir='test_out_folder')

item_layout = layout_mgr.item_layout(
    item, title=title, parent_collection=collections[0])
stac_item = Item(id=item['id'],
                 geometry=item['geometry'],
                 bbox=item['bbox'],
                 properties=item['properties'],
                 links=item_layout.get_links(),
                 assets=item['assets'])

item_layout.write(stac_item, working_dir='test_out_folder')
```

Here, the layout manager incrementally adds the catalog, collection and items stacs entries into the test output folder.
The layout, is aware of the stacs elements inter-connection, to it ss also in charge of producing the links entries into stac elements.

## Drb extended Authentication
We can see in the quick start here before the BasicAuth authentication 
is managed with `requests.auth` package. 
drb-impl-http provides simple and easy way to manage OAuth2 service connection:
```python
import os
from drb_impl_http.oauth2.HTTPOAuth2 import HTTPOAuth2
from drb_impl_odata import ODataServiceNode

# Basic auth cretentials
SERVICE = os.environ.get('gss_service')
USERNAME = os.environ.get('gss_username')
PASSWORD = os.environ.get('gss_password')

# OAuth 2.0 credentials (Client secret can be None)
OAUTH_TOKEN_SERVICE = os.environ.get('auth_token_service')
OAUTH_CLIENT_ID = os.environ.get('auth_client_id')
OAUTH_CLIENT_SECRET = os.environ.get('auth_client_secret')

auth = HTTPOAuth2(USERNAME, PASSWORD, OAUTH_TOKEN_SERVICE, 
                  OAUTH_CLIENT_ID, OAUTH_CLIENT_SECRET)

node = ODataServiceNode(SERVICE, auth)
...
```

## OData Filtering
OData standard API is defined as a standard API by OASIS working group.
The online reference is available at https://www.oasis-open.org/committees/tc_home.php?wg_abbrev=odata#overview

As a quick reference, this chapter highlight a set of complex filters:
### Filter by Product property
Product properties:

| Property Name          | Type | sample | 
|------------------------|-----------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Id                     | UUID      | `0723dde6-7ca2-3a33-a8fb-d70621b542f3`                                                                                                                                                                            |
| Name                   | String    | `S2A_OPER_MSI_L0__GR_SGS__20180420T195109_S20180420T163556_D06_N02.06.tar`                                                                                                                                        |
| ContentType            | Strig     | `application/x-tar`                                                                                                                                                                                               |
| ContentLength          | Integer   | 	`18388992`                                                                                                                                                                                                       |
| OriginDate             | 	DateTime | `2021-01-03T03:41:44.328Z`                                                                                                                                                                                        |
| PublicationDate        | 	DateTime | 	`2021-08-29T08:07:03.798Z`                                                                                                                                                                                       |
| ModificationDate       | 	DateTime | 	`2021-08-29T08:07:03.798Z`                                                                                                                                                                                       |
| Online                 | Boolean   | 	`true`                                                                                                                                                                                                           |
| EvictionDate	          | 	DateTime | `2022-08-29T08:07:03.858Z`                                                                                                                                                                                        |
| Checksum/Algorithm     | String    | 	`MD5`                                                                                                                                                                                                            |
| Checksum/Value         | String | 	`5e2c3beb7a1493aabdd5ba7e43f11b9c`                                                                                                                                                                               |
| Checksum/ChecksumDate  | DateTime | 	`2021-08-29T08:07:03.798Z`                                                                                                                                                                                       |
| ContentDate/Start      | DateTime | 	`2018-04-20T16:35:56.000Z`                                                                                                                                                                                       |
| ContentDate/End        | DateTime | 	`2018-04-20T16:35:56.000Z`                                                                                                                                                                                       |
| Footprint              | WKT | 	`geography'SRID=4326;Polygon((-87.1046083578953 27.0342869996124,-87.0091398629774 27.3981345441031,-87.2587972912068 27.4487803251302,-87.3532717079262 27.0846594328354,-87.1046083578953 27.0342869996124))'` |


*Filters examples*
```python
filter = "startswith(Name, 'S2A_OPER_MSI_L0')"
filter = "endswith(Name, '.tar')"
filter = "contains(Name, '_OPER_')"
```

### Filter a Region of area
The ROI filter can matches the Product `Footprint` property to manage query:

```python
filter="OData.CSC.Intersects(location=Footprint,area=geography’SRID=4326;" \
       "POLYGON((-2.9396525469094588 47.73283920337374," \
       "         -2.9001931876733265 47.73283920337374," \
       "         -2.9001931876733265 47.74770130750534," \
       "         -2.9396525469094588 47.74770130750534," \
       "         -2.9396525469094588 47.73283920337374))')"
```
This query intersects Footprint location with the given polygon.
details are available [here](https://www.odata.org/blog/geospatial-properties/)

### Filter by Product Attribute.
Product catalogue carry out a lot of additional metadata available via 
`Attributes` entity. Cross querying such information with product os a bit 
more complex than querying product properties. For example The following OData
content
```json
{
  "@odata.context": "$metadata#Products(Attributes())",
  "value": [
    {
      "@odata.mediaContentType": "application/octet-stream",
      "Id": "15182eaa-9032-37f4-b75b-8e7b4936152a",
      "Name": "S2A_OPER_MSI_L0__GR_EPAE_20180907T011236_S20180907T000534_D10_N02.06.tar",
      "ContentType": "application/x-tar",
      "ContentLength": 18388992,
      "OriginDate": "2021-01-23T03:49:49.518Z",
      "PublicationDate": "2021-09-08T16:14:28.300Z",
      "ModificationDate": "2021-09-08T16:14:28.300Z",
      "Online": true,
      "EvictionDate": "2022-09-08T16:14:28.371Z",
      "Checksum": [
        {
          "Algorithm": "MD5",
          "Value": "50a9858414170f8db5d5ad3919ce09db",
          "ChecksumDate": "2021-09-08T16:14:28.300Z"
        }
      ],
      "ContentDate": {
        "Start": "2018-09-07T00:05:34.000Z",
        "End": "2018-09-07T00:05:34.000Z"
      },
      "Footprint": "geography'SRID=4326;Polygon((151.671006087229 -32.4783949180862,151.767485366154 -32.1112334504858,151.498185359656 -32.050978632743,151.401357062066 -32.4172923986598,151.671006087229 -32.4783949180862))'",
      "Attributes": [
        {
          "@odata.type": "#OData.CSC.StringAttribute",
          "Name": "instrumentShortName",
          "ValueType": "String",
          "Value": "MSI"
        },
        {
          "@odata.type": "#OData.CSC.StringAttribute",
          "Name": "productGroupId",
          "ValueType": "String",
          "Value": "GS2A_20180907T000241_016758_N02.06"
        },
        {
          "@odata.type": "#OData.CSC.StringAttribute",
          "Name": "temporalExtentBeginPosition",
          "ValueType": "String",
          "Value": "2018-09-07T00:05:34.000000Z"
        },
        {
          "@odata.type": "#OData.CSC.StringAttribute",
          "Name": "beginningDateTime",
          "ValueType": "String",
          "Value": "2018-09-07T00:05:34.000000Z"
        },
        {
          "@odata.type": "#OData.CSC.StringAttribute",
          "Name": "platformShortName",
          "ValueType": "String",
          "Value": "SENTINEL-2"
        },
        {
          "@odata.type": "#OData.CSC.StringAttribute",
          "Name": "qualityStatus",
          "ValueType": "String",
          "Value": "NOMINAL"
        },
        {
          "@odata.type": "#OData.CSC.StringAttribute",
          "Name": "geographicBoundingBoxWestBoundLongitude",
          "ValueType": "Double",
          "Value": 151.401357062066
        }
...
```

Can be query with the follwing filter:
```python
filter = "Attributes/OData.CSC.StringAttribute/any(" \
            "att:att/Name eq 'productType' and " \
            "att/OData.CSC.StringAttribute/Value eq 'MSI_L0__GR')"
```

## Organization of these sources
source code contains the following files and folders.

```commandline
.
├── Makefile
├── MANIFEST.in
├── README.md
├── requirements-doc.txt
├── requirements.txt
├── resources
│   ├── __init__.py
│   └── schema
│       ├── __init__.py
│       ├── basics.json
│       ├── catalog.json
│       ├── collection.json
│       ├── datetime.json
│       ├── instrument.json
│       ├── item.json
│       ├── licensing.json
│       ├── provider.json
├── setup.cfg
├── setup.py
├── drb_stac
│   ├── __init__.py
│   ├── odata_stac.py
│   ├── schema
│   │   └── model.py
│   ├── stac_model.py
│   ├── stac.py
│   └── _version.py
├── tests
│   └── test_stac_model.py
└── versioneer.py

```
 The root level contains build and (gitlab) pipeline files used to manage source code conformity to the projects requirements.
It also includes version management files, based on versionneer tool: the version is automatically managed from git tags.

`resources` folder contains resources copied from STAC reference repository. It mainly contains json schemas used to validate and create python model of STACs elements (Catalogue, Collection, Item). Schemas are based on STAC version 1.0.0 (https://schemas.stacspec.org/)

`drb_stac` folder contains python source code used to manage stac data model ([drb_stac/stac_model.py](drb_stac/stac_model.py)), validation([drb_stac/schema/model.py](drb_stac/schema/model.py)), model builder([drb_stac/stac.py](drb_stac/stac.py)), Odata element extractor ([drb_stac/odata_stac.py](drb_stac/odata_stac.py)) and layout manager([drb_stac/layout.py](drb_stac/layout.py)).

## Note about design

The very first objective of this development consists in demonstrate DRB python API facility. So [drb_stac/odata_stac.py](drb_stac/odata_stac.py) contains an implementation of STAC Item generation with Odata API.
The rest of the source code manage STAC model, validation and generation layout.

The [Stac](drb_stac/stac.py) interface is restricted to build catalogues, collections and items data structures. The Reset API has not been handeled.

# Sample STAC Service API
The module also includes a basic service API, running and HTTP service dedicated to serve STAC API.
It shall be configured via environment variable:

| variable         | description                                            |
|------------------|--------------------------------------------------------|
| `SOURCE_DATA`    | local path where are stored STAC JSON generated  files |
| `STAC_ROOT_URL`  | URL to reach Stac API                                  |


It can be executed with command line:
```bash
export SOURCE_DATA="$HOME/stac/catalog"
export STAC_ROOT_URL="https://stac.gael-systems.com/stac"

python3 drb_stac/api/service.py
```

