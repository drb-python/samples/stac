import unittest
import json
import importlib.resources as pkg
import drb_stac.odata_stac as odata
from copy import deepcopy
from drb.utils.logical_node import DrbLogicalNode
from tests import data
from tenacity import wait_none, stop_after_attempt,  RetryError

odata.item_content_from_odata_node.retry.wait = wait_none
odata.item_content_from_odata_node.retry.stop = stop_after_attempt(1)


class OdataNode(DrbLogicalNode):
    """
    Simulates OData node
    """
    def __init__(self, odata: dict,
                 attrs: dict = {},
                 service_name: str = 'tests'):
        super().__init__(service_name)
        self.name = odata['Name']
        for key, value in odata.items():
            self.attributes[key, None] = value

        attribute_child = DrbLogicalNode('Attributes')
        if attrs is None or len(attrs) == 0:
            attribute_child.children = [DrbLogicalNode(
                attr['Name'], name=attr['Name'], value=attr['Value'])
                for attr in odata.get('Attributes')]
        else:
            attribute_child.children = [DrbLogicalNode(
                key, name=key, value=value) for key, value in attrs.items()]

        self.children = [attribute_child]

    def reset_child(self):
        self.children = []


def do_node_reset_child(product: dict):
    node = OdataNode(product)
    node.reset_child()
    return node


class TestStacODataItem(unittest.TestCase):
    '''
    file odata_no_attrs.json extracted with query
        $filter=startswith(Name,'S2A')&$top=10&$format=json&$expand=Attributes
    '''
    odata_no_attrs = json.loads(pkg.read_text(data, 'odata_no_attrs.json'))
    odata_response = json.loads(pkg.read_text(data, 'odata.json'))

    def test_odata_item(self):
        # No atttibutes
        product = OdataNode(self.odata_no_attrs['value'][0])
        item = next(odata.item_content_from_odata_nodes([product]))
        self.assertTrue(
            product.name.startswith(item['properties'].get('title')))
        self.assertIsNone(item['properties'].get('description'))
        self.assertIsNone(item['properties'].get('eo:cloud_cover'))
        self.assertIsNone(item['properties'].get('eo:orbit'))

        product = OdataNode(self.odata_no_attrs['value'][0])
        product.reset_child()
        item = next(odata.item_content_from_odata_nodes([product]))
        self.assertIsNone(item['properties'].get('title'))

        attrs = {'resourceTitle': 'TITLE', 'resourceAbstract': 'DESCRIPTION'}
        product = OdataNode(self.odata_no_attrs['value'][0], attrs=attrs)
        item = next(odata.item_content_from_odata_nodes([product]))
        self.assertEqual(item['properties'].get('description'),
                         '<b>TITLE</b>:<br/>DESCRIPTION')
        self.assertIsNone(item['properties'].get('eo:orbit'))

    def test_odata_item_missing_attribute(self):
        odata_tuple = deepcopy(self.odata_no_attrs['value'][0])
        odata_tuple.pop('Footprint')
        product = OdataNode(odata_tuple)
        with self.assertRaises(expected_exception=RetryError):
            item = next(odata.item_content_from_odata_nodes([product]))
            self.assertIsNone(item['properties'].get('title'))

    def test_odata_items_missing_mandatory_entry(self):
        self.assertEqual(len(self.odata_no_attrs['value']), 10)
        products = [OdataNode(product)
                    for product in self.odata_no_attrs['value']]
        count = 0
        for item in odata.item_content_from_odata_nodes(products):
            print(json.dumps(item, default=str))
            count += 1
        self.assertEqual(count, 10)

    def test_odata_items_no_attrs_entry(self):
        self.assertEqual(len(self.odata_no_attrs['value']), 10)
        products = [do_node_reset_child(product)
                    for product in self.odata_no_attrs['value']]
        count = 0
        for item in odata.item_content_from_odata_nodes(products):
            print(json.dumps(item, default=str))
            count += 1
        self.assertEqual(count, 10)

    def test_odata_simulated(self):
        self.assertEqual(len(self.odata_response['value']), 10)
        products = [OdataNode(product)
                    for product in self.odata_response['value']]
        count = 0
        for item in odata.item_content_from_odata_nodes(products):
            print(json.dumps(item, default=str))
            count += 1
        self.assertEqual(count, 10)
