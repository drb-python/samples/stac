import unittest
from drb_stac.schema.model import ModelBuilder
from drb_stac.stac_model import Catalog, Collection, Item
from drb_stac.stac_model import Extent, SpatialExtent, TemporalExtent
from drb_stac.stac_model import Link, LinkRelEnum
from datetime import datetime, timezone


class TestStacModel(unittest.TestCase):
    link = [Link(rel=LinkRelEnum.SELF, href="http://test/pathhref")]
    bbox = [-180.0, -90.0, 180.0, 90.0]
    interval = [datetime.now(timezone.utc).isoformat(), None]

    extent = Extent(spatial=SpatialExtent(bbox=[bbox]),
                    temporal=TemporalExtent(interval=[interval]))
    providers = [{
        "name": "GAEL Systems",
        "description": "Company name",
        "roles": ["licensor", "processor", "host"],
        "url": "https://www.gael-systems.com"
    }]

    license = "AGPL-3.0-or-later"

    geometry = {
        "type": "Polygon",
        "coordinates": [[
            [-122.308150179, 37.488035566],
            [-122.597502109, 37.538869539],
            [-122.576687533, 37.613537207],
            [-122.2880486, 37.562818007],
            [-122.308150179, 37.488035566]]]}

    properties = {
        "datetime": "2016-05-03T13:22:30.040Z",
        "title": "A CS3 item",
        "license": license,
        "providers": providers,
    }

    assets = {
        "name": {
            "href": "https://source/of/data",
            "title": "The data source"
        }
    }

    def test_model_builder(self):
        builder = ModelBuilder()
        self.assertIsNotNone(builder)

    def test_model_builder_get_catalog_class(self):
        builder = ModelBuilder()
        catalog_class = builder.get_catalog_class()
        self.assertIsNotNone(catalog_class)

    def test_model_builder_get_collection_class(self):
        builder = ModelBuilder()
        collection_class = builder.get_collection_class()
        self.assertIsNotNone(collection_class)

    def test_model_builder_get_item_class(self):
        builder = ModelBuilder()
        item_class = builder.get_item_class()
        self.assertIsNotNone(item_class)

    def test_model_builder_get_catalog(self):
        builder = ModelBuilder()
        catalog_class = builder.get_catalog_class()
        catalog = catalog_class(
            stac_version="1.0.0", type="Catalog",
            id="CatalogID", description="noop",
            links=[lnk.dict_data for lnk in self.link],
            stac_extensions=[], title="")
        self.assertIsNotNone(catalog)

    def test_model_builder_get_collection(self):
        builder = ModelBuilder()
        collection_class = builder.get_collection_class()
        collection = collection_class(
            stac_version="1.0.0", type="Collection", id="CollectionID",
            description="noop", links=[lnk.dict_data for lnk in self.link],
            stac_extensions=[], title="", license=self.license,
            extent=self.extent.dict_data, keywords=["keywords"],
            providers=self.providers)
        self.assertIsNotNone(collection)

    def test_model_builder_get_item(self):
        builder = ModelBuilder()
        item_class = builder.get_item_class()
        item = item_class(
            stac_version="1.0.0", type="Feature", id="itemID",
            description="noop", links=[lnk.dict_data for lnk in self.link],
            stac_extensions=[], title="", license="AGPL-3.0-or-later",
            extent=self.extent, keywords=["keywords"], bbox=self.bbox,
            providers=self.providers, geometry=self.geometry,
            properties=self.properties, assets=self.assets)
        self.assertIsNotNone(item)

    def test_stac_model_catalog(self):
        catalog = Catalog(id="CatalogID", description="Missing description!",
                          links=self.link, title="My TITLE")
        self.assertEqual(catalog['id'], 'CatalogID')
        self.assertEqual(catalog['description'], 'Missing description!')
        self.assertEqual(catalog['title'], 'My TITLE')
        self.assertEqual(catalog['links'][0]['href'], 'http://test/pathhref')

    def test_stac_model_collection(self):
        collection = Collection(
            id="CollectionID", description="Missing description!",
            license=self.license, extent=self.extent,
            links=self.link)
        self.assertEqual(collection['id'], 'CollectionID')
        self.assertEqual(collection['description'], 'Missing description!')
        self.assertEqual(collection['license'], self.license)
        self.assertEqual(collection['links'][0]['href'],
                         'http://test/pathhref')

    def test_stac_model_item(self):
        item = Item(id="ItemID", geometry=self.geometry, bbox=self.bbox,
                    properties=self.properties, links=self.link,
                    assets=self.assets)
        self.assertEqual(item['id'], 'ItemID')
        self.assertEqual(item['links'][0]['href'], 'http://test/pathhref')
        self.assertEqual(item['geometry']['type'], 'Polygon')
