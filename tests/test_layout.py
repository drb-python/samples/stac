import unittest
import os
from urllib.parse import urljoin
import json
from drb_stac.layout.stac_layout import StacLayout
from drb_stac.stac_model import Collection, Extent, SpatialExtent,\
    TemporalExtent, Link, LinkRelEnum, Item
from datetime import datetime, timezone
import importlib.resources as pkg
from tests import data


class TestStacLayout(unittest.TestCase):
    root = "https://stac.gael-systems.com"
    root_title = "Gael STAC repository"

    extents = Extent(
        spatial=SpatialExtent(bbox=[[-180.0, -90.0, 180.0, 90.0]]),
        temporal=TemporalExtent(
            interval=[[datetime.utcnow().replace(tzinfo=timezone.utc).
                       isoformat(), None]]))

    collection = Collection(id="collection_id",
                            description="Test collection",
                            license="custom_lic",
                            extent=extents,
                            links=[])

    @staticmethod
    def item():
        item_dict = json.loads(pkg.read_text(data, 'core-item.json'))
        return Item(id=item_dict['id'],
                    geometry=item_dict['geometry'],
                    bbox=item_dict['bbox'],
                    properties=item_dict['properties'],
                    links=[Link(href=link['href'],
                                rel=LinkRelEnum(link['rel']),
                                type=link.get('type'),
                                title=link.get('title'))
                           for link in item_dict['links']],
                    assets=item_dict['assets'],
                    collection="nocollection")

    def test_filenames(self):
        # Check defaults
        self.assertEqual(StacLayout().root_path, 'file:' + os.getcwd())
        self.assertIsNone(StacLayout().root_type)
        self.assertIsNone(StacLayout().root_title)

        stac_layout = StacLayout(root_href=self.root,
                                 root_title=self.root_title)
        self.assertEqual(stac_layout.root_path, self.root)
        self.assertEqual(stac_layout.root_title, self.root_title)

        self.assertEqual(stac_layout._item_filename('it_id'), "it_id.json")
        self.assertEqual(stac_layout._catalog_default_href(),
                         os.path.join(self.root, "catalog.json"))
        self.assertEqual(stac_layout._collection_default_href(
            collection_identifier="Collect").replace('\\', '/'),
            urljoin(self.root, "Collect/collection.json"))
        self.assertEqual(stac_layout._item_as_child_default_href(
            collection_identifier='258'), os.path.join(self.root, '258'))
        self.assertEqual(stac_layout._item_default_href(
            item_identifier='zdt',
            collection_identifier="Collect").replace('\\', '/'),
            urljoin(self.root, 'Collect/zdt.json'))

    def test_catalogue_layout(self):
        identifier = 'catalog_drb'
        title = 'DRB Catalogue demonstration'

        layout = StacLayout(self.root, root_title="Gael STAC repository").\
            catalog_layout(title=title, collections=[self.collection])

        self.assertIsNotNone(layout)
        self.assertIsNotNone(layout.self)
        # 1 child collection + 1 self + 1 root = 3 entries expected
        self.assertEqual(len(layout.get_links()), 4)

    def test_collection_layout(self):
        identifier = 'collection_by_roi'
        title = 'DRB Collection demonstration'

        layout = StacLayout(self.root, root_title="Gael STAC repository").\
            collection_layout(parent_title="catalog number one",
                              identifier=identifier, title=title)

        self.assertIsNotNone(layout)
        self.assertIsNotNone(layout.self)
        print(layout.get_links())
        # 1 self
        # 1 root
        # 1 parent catalogue
        # = 3 links
        self.assertEqual(len(layout.get_links()), 4)

    def test_item_layout(self):
        item = self.item()
        layout = StacLayout(self.root, root_title="Gael STAC repository"). \
            item_layout(item=item, parent_collection=self.collection)

        self.assertIsNotNone(layout)
        self.assertIsNotNone(layout.self)
        # 1 self
        # 1 root
        # 1 parent collection
        # 1 collection
        # = 4 links
        self.assertEqual(len(layout.get_links()), 4)

    def test_item_layout_more(self):
        item = self.item()
        layout = StacLayout(self.root, root_title="Gael STAC repository"). \
            item_layout(item=item, parent_collection=self.collection)

        layout.add_item(href="/item", link_type="application/xml",
                        title="Additional item")

        layout.add_license(href="/license", link_type="license/gnu+text",
                           title="License link")

        layout.add_derived_from(href="/derived",
                                link_type="application/octet+stream",
                                title="derived link")

        self.assertIsNotNone(layout)
        self.assertIsNotNone(layout.self)
        # 1 self
        # 1 root
        # 1 parent collection
        # 1 collection
        # + 3 items
        self.assertEqual(len(layout.get_links()), 7)
