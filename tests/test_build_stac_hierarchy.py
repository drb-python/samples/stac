import unittest
import json
from datetime import datetime, timedelta, timezone
import importlib.resources as pkg
import drb_stac.odata_stac as odata

from drb_stac.stac_model import Link, LinkRelEnum, Role, Provider, Extent, \
    SpatialExtent, TemporalExtent, Catalog, Collection, Item

from tests import data
from tests.test_odata_item import OdataNode


class TestStacHierarchy(unittest.TestCase):
    root_url = 'https://www.gael.fr/test'
    now = datetime.utcnow().replace(tzinfo=timezone.utc)
    previous_now = now - timedelta(hours=1)

    description = pkg.read_text(data, "sentinel_description.txt")
    summary = json.loads(pkg.read_text(data, "s2_summary.json"))
    keywords = json.loads(pkg.read_text(data, "s2_keyword.json"))
    stac_extensions = json.loads(
        pkg.read_text(data, "stac_default_extensions.json"))

    def test_build_catalog(self):
        # Catalog of products over Plumergat
        links = [
            Link(href=f'{self.root_url}/catalog.json', rel=LinkRelEnum.SELF),
            Link(href=f'{self.root_url}', rel=LinkRelEnum.ROOT),
            Link(href=f'{self.root_url}', rel=LinkRelEnum.PARENT),
            Link(href=f'{self.root_url}/plum_collection_s2.json',
                 rel=LinkRelEnum.CHILD),
            Link(href=f'{self.root_url}/plum_collection_s1.json',
                 rel=LinkRelEnum.CHILD)]

        catalog = Catalog(
            id='catalog',
            description='Catalog demonstrating DRB python on-the-fly'
                        ' creation of STACs items',
            title='Plumergat Location Catalogue',
            links=links)
        print(json.dumps(catalog._dict()))

    def test_build_collections(self):
        roles = [Role.host, Role.producer]
        providers = [Provider(name="gss-catalogue",
                              description='Online Odata GSS catalog',
                              roles=roles,
                              url="https://www.gael.fr/test")]
        extent = Extent(
            spatial=SpatialExtent(bbox=[[-180.0, -90.0, 180.0, 90.0]]),
            temporal=TemporalExtent(
                interval=[[self.now.isoformat(),
                           self.previous_now.isoformat()]]))

        links = [
            Link(href=f'{self.root_url}', rel=LinkRelEnum.ROOT),
            Link(href=f'{self.root_url}/catalog.json',
                 rel=LinkRelEnum.PARENT),
            Link(href=f'{self.root_url}/plum_collection_s2.json',
                 rel=LinkRelEnum.SELF)]

        collection = Collection(id="plum_collection_s2",
                                stac_extensions=self.stac_extensions,
                                title="Sentinel-2 over Plumergat",
                                description=self.description,
                                keywords=self.keywords,
                                license='proprietary',
                                providers=providers,
                                extent=extent,
                                summaries=self.summary,
                                links=links)

        print(json.dumps(collection._dict()))

    def test_build_item(self):
        # Simulate OData request
        odata_no_attrs = json.loads(pkg.read_text(data, 'odata.json'))
        product = OdataNode(odata_no_attrs['value'][0])
        # Build info from odata node.
        stac_info = next(odata.item_content_from_odata_nodes([product]))

        links = [
            Link(href=f'{self.root_url}', rel=LinkRelEnum.ROOT),
            Link(href=f'{self.root_url}/collection.json',
                 rel=LinkRelEnum.PARENT),
            Link(href=f'{self.root_url}/{stac_info["id"]}.json',
                 rel=LinkRelEnum.SELF),
            Link(href=f'{self.root_url}/collection.json',
                 rel=LinkRelEnum.COLLECTION),
        ]

        item = Item(id=stac_info['id'],
                    geometry=stac_info['geometry'],
                    bbox=stac_info['bbox'],
                    properties=stac_info['properties'],
                    links=links,
                    assets=stac_info['assets'],
                    collection="noname")
        print(json.dumps(item._dict()))
