import datetime
import unittest
import importlib.resources as pkg
import json
import os
from typing import Union

import tests.data as data
from drb_stac.stac_model import Catalog, Collection, Item, Link, LinkRelEnum
from drb_stac.stac_model import Extent, SpatialExtent, TemporalExtent
from drb_stac.layout.stac_layout import StacLayout
from drb_stac.odata_stac import item_content_from_odata_node
from drb.abstract_node import Predicate
from drb_impl_odata import ODataQueryPredicate, ODataServiceNodeCSC
from drb_impl_http.oauth2.HTTPOAuth2 import HTTPOAuth2
from tests.test_odata_item import OdataNode

# OData service settings
SERVICE = os.environ.get(
    'gss_service',
    'https://beta.databridge.gael-systems.com/gss-catalogue')
USERNAME = os.environ.get('gss_username')
PASSWORD = os.environ.get('gss_password')
OAUTH_TOKEN_SERVICE = os.environ.get('auth_token_service')
OAUTH_CLIENT_ID = os.environ.get('auth_client_id')
OAUTH_CLIENT_SECRET = os.environ.get('auth_client_secret')

description = pkg.read_text(data, "sentinel_description.txt")
summaries = json.loads(pkg.read_text(data, "s2_summary.json"))
keywords = json.loads(pkg.read_text(data, "s2_keyword.json"))

stac_extensions = [
    'https://stac-extensions.github.io/eo/v1.0.0/schema.json',
    'https://stac-extensions.github.io/projection/v1.0.0/schema.json',
    'https://stac-extensions.github.io/view/v1.0.0/schema.json',
    'https://stac-extensions.github.io/file/v2.1.0/schema.json'
]


# CATALOGUE
catalog_id = 'DEMO_CATALOG_V1'
catalog_title = 'DRB-PYTHON STAC DEMO CATALOG '
catalog_date = datetime.datetime.now().strftime("%a %b %d %H:%M:%S %Y")

catalog_description = description + f'\nGael Systems - ' + \
                      f'Created {catalog_date}'

# COLLECTIONS
collection1_id = "DEMO_COLLECTION_1_V1"
collection1_title = "Sentinel-2 collection published in 30th, Sept 2021"
collection1_predicate = ODataQueryPredicate(
    filter="startswith(Name, 'S2A') and "
           "PublicationDate gt 2021-09-30T00:59:00.000Z and "
           "PublicationDate lt 2021-09-30T01:00:00.000Z")

collection1_description = \
    "Sample S2 STAC Collection from Odata service by queried by name " \
    "in a range of date with the filter: \n" \
    "```\n" \
    "$filter=startswith(Name, 'S2A') and\n\n" \
    "  PublicationDate gt 2021-09-30T00:59:00.000Z and\n\n" \
    "  PublicationDate lt 2021-09-30T01:00:00.000Z\n\n" \
    "```"
collection1_extent = Extent(
    spatial=SpatialExtent(bbox=[[-180.0, -90.0, 180.0, 90.0]]),
    temporal=TemporalExtent(interval=[[
        "2021-09-30T00:59:00.000Z",
        "2021-09-30T01:00:00.000Z"]]))

collection2_id = "DEMO_COLLECTION_2_V1"
collection2_title = "Sentinel-2 collection over ROI"
collection2_predicate = ODataQueryPredicate(
    filter="startswith(Name, 'S2A') and "
           "OData.CSC.Intersects(location=Footprint,area=geography’SRID=4326;"
           "POLYGON((-2.9396525469094588 47.73283920337374,"
           "         -2.9001931876733265 47.73283920337374,"
           "         -2.9001931876733265 47.74770130750534,"
           "         -2.9396525469094588 47.74770130750534,"
           "         -2.9396525469094588 47.73283920337374))")
collection2_description = \
    "Sample S2 STAC Collection over ROI with the filter: \n" \
    "```\n" \
    "$filter=startswith(Name, 'S2A') and\n" \
    "  OData.CSC.Intersects(location=Footprint,area=geography’SRID=4326;\n" \
    "POLYGON((-2.9396525469094588 47.73283920337374,\n" \
    "         -2.9001931876733265 47.73283920337374,\n" \
    "         -2.9001931876733265 47.74770130750534,\n" \
    "         -2.9396525469094588 47.74770130750534,\n" \
    "         -2.9396525469094588 47.73283920337374))')\n" \
    "```"

collection2_extent = Extent(
    spatial=SpatialExtent(bbox=[[-2.94, 47.73, -2.90, 47.75]]),
    temporal=TemporalExtent(interval=[["2015-06-23T01:52:00.000Z", None]]))

collection3_id = "DEMO_COLLECTION_3_V1"
collection3_title = "Sentinel-2 collection with less than 80% of cloud cover."
collection3_predicate = ODataQueryPredicate(
    filter="startswith(Name, 'S2A') and "
           "Attributes/OData.CSC.StringAttribute/any("
           "    att:att/Name eq 'cloudCover' and "
           "    att/OData.CSC.DoubleAttributes/Value gt 80)")
collection3_description = \
    "Sample S2 STAC Collection of product with less than 80% of cloud cover." \
    "The filter: \n" \
    "```\n"        \
    "$filter=startswith(Name, 'S2A') and\n"           \
    "    Attributes/OData.CSC.StringAttribute/any(\n" \
    "        att:att/Name eq 'cloudCover' and \n"      \
    "        att/OData.CSC.DoubleAttributes/Value gt 80)\n" \
    "```"

collection3_extent = Extent(
    spatial=SpatialExtent(bbox=[[-180.0, -90.0, 180.0, 90.0]]),
    temporal=TemporalExtent(interval=[["2015-06-23T01:52:00.000Z", None]]))


class TestStacGenerate(unittest.TestCase):
    @staticmethod
    def get_odata_node_from_predicate(
            predicate: Union[Predicate, str, int, slice, tuple]):
        auth = HTTPOAuth2(USERNAME, PASSWORD, OAUTH_TOKEN_SERVICE,
                          OAUTH_CLIENT_ID, OAUTH_CLIENT_SECRET)
        svc = ODataServiceNodeCSC(SERVICE, auth)
        return svc[predicate]

    def test_full_layout(self):
        layout_mgr = StacLayout(
            root_href="${STAC_ROOT_URL}",
            root_title="STAC repository")

        collection1_layout = layout_mgr.collection_layout(
            parent_title=catalog_title,
            identifier=collection1_id,
            title=collection1_title)
        collection2_layout = layout_mgr.collection_layout(
            parent_title=catalog_title,
            identifier=collection2_id,
            title=collection2_title)
        collection3_layout = layout_mgr.collection_layout(
            parent_title=catalog_title,
            identifier=collection3_id,
            title=collection3_title)

        collections = [
            Collection(id=collection1_id,
                       description=collection1_description,
                       license="proprietary",
                       extent=collection1_extent,
                       links=collection1_layout.get_links(),
                       stac_extensions=stac_extensions,
                       title=collection1_title,
                       keywords=keywords,
                       providers=[],  # Not implemented (dict can be used)
                       summaries=summaries,
                       assets={}),  # Not implemented (dict can be used)
            Collection(id=collection2_id,
                       description=collection2_description,
                       license="proprietary",
                       extent=collection2_extent,
                       links=collection2_layout.get_links(),
                       stac_extensions=stac_extensions,
                       title=collection2_title,
                       keywords=keywords,
                       providers=[],  # Not implemented (dict can be used)
                       summaries=summaries,
                       assets={}),  # Not implemented (dict can be used)
            Collection(id=collection3_id,
                       description=collection3_description,
                       license="proprietary",
                       extent=collection3_extent,
                       links=collection3_layout.get_links(),
                       stac_extensions=stac_extensions,
                       title=collection3_title,
                       keywords=keywords,
                       providers=[],  # Not implemented (dict can be used)
                       summaries=summaries,
                       assets={})  # Not implemented (dict can be used)
        ]

        catalog_layout = layout_mgr.catalog_layout(
            title=catalog_title, collections=collections)
        stac_catalog = Catalog(id=catalog_id, title=catalog_title,
                               links=catalog_layout.get_links(),
                               description=catalog_description)

        # Simulated OData Response node (Unfortunatly predicate not managed)
        # nodes = self.get_odata_node_from_predicate(collection1_predicate)
        nodes = json.loads(pkg.read_text(data, 'odata.json'))['value']
        for node in nodes:
            item_url = f"{SERVICE}/Products({node['Id']})"
            item = item_content_from_odata_node(OdataNode(
                node, service_name=item_url))

            title = item.get('properties').get('title') \
                if item.get('properties') is not None else None
            item_layout = layout_mgr.item_layout(
                item, title=title, parent_collection=collections[0])
            stac_item = Item(id=item['id'],
                             geometry=item['geometry'],
                             bbox=item['bbox'],
                             properties=item['properties'],
                             links=item_layout.get_links(),
                             stac_extensions=stac_extensions,
                             assets=item['assets'],
                             collection=collections[0]['id'])

            item_layout.write(stac_item, working_dir='test_out')
            collection1_layout.add_item(href=item_layout.self.href,
                                        link_type=item_layout.self.type,
                                        title=item_layout.self.title)

        # Collection Dict shall be udated after item updates.
        collections[0]['links'] = StacLayout.dict_data(
            collection1_layout.get_links())

        collection1_layout.write(collections[0], working_dir='test_out')
        collection2_layout.write(collections[1], working_dir='test_out')
        collection3_layout.write(collections[2], working_dir='test_out')
        catalog_layout.write(stac_catalog, working_dir='test_out')
        # Now browse the StacLayout path...
