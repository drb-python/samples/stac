import unittest
from drb_stac.api.utils import replace_all_variable


class TestService(unittest.TestCase):
    def test_utils_replacement(self):
        environ = {
            "VARIABLE1": "VALUE1",
            "VARIABLE2": "VALUE2",
            "VARIABLE3": "VALUE3"
        }
        test_to_update = "The VARIABLE1=${VARIABLE1} whereas " \
                         "VARIABLE2=${VARIABLE2} and VARIABLE3=${VARIABLE3} " \
                         "but VAR_NONE=${VAR_NONE}"
        expected = "The VARIABLE1=VALUE1 whereas VARIABLE2=VALUE2 and " \
                   "VARIABLE3=VALUE3 but VAR_NONE="

        self.assertEqual(expected,
                         replace_all_variable(test_to_update, environ))
