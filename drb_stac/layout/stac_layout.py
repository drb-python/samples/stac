import errno
import json

from drb_stac.stac_model import Stac, Collection, Item, Link, LinkRelEnum
from typing import List, Union
import os

"""
Layout aims to implement STAC layout regarding the catalogue/colletion/item
structure.

"""


class Layout:
    def __init__(self, href: str, link_type: str = None, title: str = None):
        self.self: Link = None
        self.root: List[Link] = []
        self.parent: List[Link] = []
        self.child: List[Link] = []
        self.item: List[Link] = []
        self.license: List[Link] = []
        self.derived_from: List[Link] = []
        self.others = []

        self.self = Link(href=href,
                         rel=LinkRelEnum.SELF,
                         type=link_type,
                         title=title)

    def add_root(self, href: str, link_type: str = None, title: str = None):
        self.root.append(Link(href=href, rel=LinkRelEnum.ROOT,
                              type=link_type, title=title))

    def add_parent(self, href: str, link_type: str = None, title: str = None):
        self.parent.append(Link(href=href, rel=LinkRelEnum.PARENT,
                                type=link_type, title=title))

    def add_child(self, href: str, link_type: str = None, title: str = None):
        self.child.append(Link(href=href, rel=LinkRelEnum.CHILD,
                               type=link_type, title=title))

    def add_item(self, href: str, link_type: str = None, title: str = None):
        self.item.append(Link(href=href, rel=LinkRelEnum.ITEM,
                              type=link_type, title=title))

    def add_license(self, href: str, link_type: str = None, title: str = None):
        self.license.append(Link(href=href, rel=LinkRelEnum.LICENSE,
                                 type=link_type, title=title))

    def add_derived_from(self, href: str, link_type: str = None,
                         title: str = None):
        self.derived_from.append(Link(href=href, rel=LinkRelEnum.DERIVED_FROM,
                                      type=link_type, title=title))

    def add_others(self, href: str, rel: LinkRelEnum,
                   link_type: str = None, title: str = None):
        self.others.append(Link(href=href, rel=rel,
                                type=link_type, title=title))

    def get_links(self) -> List[Link]:
        _links = [self.self]
        if len(self.root) > 0:
            _links.extend(self.root)
        if len(self.parent) > 0:
            _links.extend(self.parent)
        if len(self.child) > 0:
            _links.extend(self.child)
        if len(self.item) > 0:
            _links.extend(self.item)
        if len(self.license) > 0:
            _links.extend(self.license)
        if len(self.derived_from) > 0:
            _links.extend(self.derived_from)
        if len(self.others) > 0:
            _links.extend(self.others)
        return _links

    @staticmethod
    def write(stac: Stac, working_dir='.'):
        """
        Write the passed Stac structure into file according to its self link
        definition.
        :param stac: the structure to save.
        :param working_dir: the output directory.
        """
        # look at this drb_stac element path
        rel_name = LinkRelEnum.SELF.value
        paths = [ln['href'] for ln in stac['links'] if ln['rel'] == rel_name]
        if len(paths) == 0:
            raise ValueError('No valid link entry in Stac structure(no self).')
        path = paths[0]
        # look at this drb_stac root path
        rel_name = LinkRelEnum.ROOT.value
        roots = [ln['href'] for ln in stac['links'] if ln['rel'] == rel_name]
        if len(roots) == 0:
            raise ValueError('No valid link entry in Stac structure(no root).')
        root = roots[0].replace(StacLayout.default_catalog_name, "")

        stac_path = _remove_lead_and_trail_slash(path.replace(root, ''))
        full_path = _prepare_dirs(working_dir, stac_path)

        with open(full_path, "w+") as fp:
            json.dump(stac.dict_data, fp, indent=4)


def _prepare_dirs(working_dir, stac_path):
    full_path = working_dir + "/" + stac_path
    if not os.path.exists(os.path.dirname(full_path)):
        try:
            os.makedirs(os.path.dirname(full_path))
        except OSError as exc:
            if exc.errno != errno.EEXIST:
                raise
    return full_path


def _remove_lead_and_trail_slash(s):
    if s.startswith('/'):
        s = s[1:]
    if s.endswith('/'):
        s = s[:-1]
    return s


class StacLayout:
    """
        In this implementation, we consider
        - an item can be referenced by one or more collections as 'parent',
        - an item as no child

        - a collection can be referenced by one or more catalogue
        - a collection child is the container of its items
           this implementation does not manage recursive collections
        - each collection is stored in it own container named with the
           collection id

        - a catalogue can reference multiple collections.

        child relations
        catalog ---> n*[collection ---> item_container --->  k*item]

        parent relations
        item ---> collection ---> catalogue

        These relationship is build using Link data model existong in STAC API.

    """
    default_collection_name = 'collection.json'
    default_catalog_name = 'catalog.json'
    default_type = 'application/json'

    root_path = None
    root_type = None
    root_title = None

    def __init__(self, root_href: str = None,
                 root_type: str = None,
                 root_title: str = None):
        if root_href is not None:
            self.root_path = root_href
        else:
            self.root_path = "file:" + os.getcwd()

        self.root_type = root_type
        self.root_title = root_title

    def _catalog_default_href(self):
        """
        Computes catalog href location from root.
        The catalog filename is fixed to `catalog.json` then stored into the
        root folder.
        :return: the href link to the json catalog dataset
        """
        return os.path.join(self.root_path, self.default_catalog_name)

    def _collection_default_href(self, collection_identifier):
        """
        Compute the drb_stac collection json href from root location.
        The collection path is computed from the collection identifier
        directory name plus fixed `collection.json` file name.

        :param collection_identifier: collection identifier
        :return: path to json drb_stac collection file.
        """
        return os.path.join(self.root_path, collection_identifier,
                            self.default_collection_name)

    def _item_as_child_default_href(self, collection_identifier: str):
        """
        Set of item can be referenced with its container path. This path is
        build from root folder plus collection identifier sub-directory.
        This can be use as child relation of collection to avoid listing all
        the items of the collection.

        :param collection_identifier: the collection stage level
        :return: the href to a collection of item.
        """
        return os.path.join(self.root_path, collection_identifier)

    @staticmethod
    def _item_filename(item_identifier):
        """
        Item filename is derived from its identifier with json extension.
        :param item_identifier: the item identifier
        :return: the drb_stac item json filename
        """
        return item_identifier + '.json'

    def _item_default_href(self,
                           item_identifier: str,
                           collection_identifier: str):
        """
        The item href is computed from its collection sub-folder (if any)
        and filename is created from its item identifier.
        :param collection_identifier: the collection sub folder
        :param item_identifier: the item identifier
        :return: href to the drb_stac item json file.
        """
        return os.path.join(self.root_path, collection_identifier,
                            self._item_filename(item_identifier))

    def catalog_layout(self,
                       collections: List[Collection],
                       href: str = None,
                       link_type: str = None,
                       title: str = None) -> Layout:
        """
        create the single layout of a catalog. Catalog references set of
        collections,
        :param collections:
        :param href:
        :param link_type:
        :param title:
        :return:
        """
        if href is None:
            href = self._catalog_default_href()

        if link_type is None:
            link_type = self.default_type

        layout = Layout(href, link_type=link_type, title=title)
        layout.add_root(href=self.root_path, title=self.root_title)

        for collection in collections:
            chref = self._collection_default_href(
                collection_identifier=collection['id'])
            clink_type = self.default_type
            ctitle = collection.get('title')
            layout.add_child(href=chref, link_type=clink_type, title=ctitle)
            # Also include COLLECTION (not include in STAC 1.0.0
            layout.add_others(href=chref, rel=LinkRelEnum.COLLECTION,
                              link_type=clink_type, title=ctitle)
        return layout

    def collection_layout(self,
                          identifier: str = None,
                          parent_title: str = None,
                          parent_type: str = None,
                          href: str = None,
                          link_type: str = None,
                          title: str = None) -> Layout:
        if href is None:
            href = self._collection_default_href(
                collection_identifier=identifier)

        if link_type is None:
            link_type = self.default_type

        layout = Layout(href=href, link_type=link_type, title=title)
        layout.add_root(href=self.root_path, title=self.root_title)

        phref = self._catalog_default_href()
        layout.add_parent(href=phref,
                          title=parent_title,
                          link_type=parent_type)

        layout.add_others(href=phref,
                          rel=LinkRelEnum.CATALOG,
                          title=parent_title,
                          link_type=parent_type)

        return layout

    def item_layout(self, item: Item, parent_collection: Collection,
                    href: str = None, link_type: str = None,
                    title: str = None) -> Layout:
        """
        :param parent_collection:
        :param title:
        :param link_type:
        :param href:
        :param item:
        :return:
        """
        if href is None:
            href = self._item_default_href(
                item_identifier=item['id'],
                collection_identifier=parent_collection['id'])

        if link_type is None:
            link_type = self.default_type

        layout = Layout(href, link_type=link_type, title=title)
        layout.add_root(href=self.root_path, title=self.root_title)

        chref = self._collection_default_href(parent_collection['id'])
        ctitle = parent_collection.get('title')
        ctype = self.default_type
        layout.add_parent(href=chref, link_type=ctype, title=ctitle)
        layout.add_others(href=chref, rel=LinkRelEnum.COLLECTION,
                          link_type=ctype, title=ctitle)
        return layout

    @staticmethod
    def dict_data(stac: Union[List[Stac], Stac]):
        if isinstance(stac, list):
            return [s.dict_data for s in stac]
        return stac.dict_data
