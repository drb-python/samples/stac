from __future__ import annotations
from typing import List
from abc import ABC, abstractmethod
from enum import Enum, auto
from drb_stac.schema.model import ModelBuilder
from dataclasses import dataclass, field

VERSION = "1.0.0"


class Stac(ABC, dict):
    _dict_data = None

    @property
    def dict_data(self):
        if self._dict_data is None:
            self._dict_data = self._dict()
        return self._dict_data

    @abstractmethod
    def _dict(self) -> dict:
        raise NotImplementedError

    @staticmethod
    @abstractmethod
    def from_dict(d: dict) -> Stac:
        raise NotImplementedError

    @abstractmethod
    def validate(self) -> bool:
        raise NotImplementedError

    def __setitem__(self, key, item):
        self.dict_data[key] = item

    def __getitem__(self, key):
        return self.dict_data[key]

    def __repr__(self):
        return repr(self.dict_data)

    def __len__(self):
        return len(self.dict_data)

    def __delitem__(self, key):
        del self.dict_data[key]

    def clear(self):
        return self.dict_data.clear()

    def copy(self):
        return self.dict_data.copy()

    def has_key(self, k):
        return k in self.dict_data

    def update(self, *args, **kwargs):
        return self.dict_data.update(*args, **kwargs)

    def keys(self):
        return self.dict_data.keys()

    def values(self):
        return self.dict_data.values()


class LinkRelEnum(Enum):
    SELF = 'self'
    ROOT = 'root'
    PARENT = 'parent'
    CHILD = 'child'
    ITEM = 'item'
    LICENSE = 'license'
    DERIVED_FROM = 'derived_from'
    CATALOG = 'catalog'
    COLLECTION = 'collection'

    @classmethod
    def _missing_(cls, value):
        return cls.CHILD


@dataclass
class Link(Stac):
    """Implements Link Stac content
    """
    href: str
    rel: LinkRelEnum
    type: str = None
    title: str = None

    def validate(self) -> bool:
        if self.href is not None and self.rel is not None:
            return True
        return False

    def _dict(self) -> dict:
        data = {'href': self.href,  'rel': self.rel.value}
        if self.type is not None:
            data['type'] = self.type
        if self.title is not None:
            data['title'] = self.title
        return data

    @staticmethod
    def from_dict(d: dict) -> Link:
        return Link(href=d['href'], rel=LinkRelEnum(d['rel']),
                    type=d.get('type'), title=d.get('title'))


@dataclass
class Provider(Stac):
    name: str
    description: str = None
    roles: list = None
    url: str = None

    def validate(self) -> bool:
        if self.name is not None:
            return True
        return False

    def _dict(self) -> dict:
        data = {'name': self.name}
        if self.description is not None:
            data['description'] = self.description
        if self.roles is not None and len(self.roles) > 0:
            data['roles'] = [rl.name for rl in self.roles]
        if self.url is not None:
            data['url'] = self.url
        return data

    @staticmethod
    def from_dict(d: dict) -> Provider:
        roles = d.get('roles')
        _roles = None
        if roles is not None and len(roles) > 0:
            _roles = [Role(role) for role in roles]

        return Provider(name=d['name'], description=d.get('description'),
                        roles=_roles, url=d.get('url'))


class Role(Enum):
    licensor = "licensor"
    producer = "producer"
    processor = "processor"
    host = "host"

    @classmethod
    def _missing_(cls, value):
        return cls.licensor


@dataclass
class SpatialExtent(Stac):
    bbox: list

    def validate(self) -> bool:
        if self.bbox is not None:
            return True
        return False

    def _dict(self) -> dict:
        return {'bbox': self.bbox}

    @staticmethod
    def from_dict(d: dict) -> SpatialExtent:
        return SpatialExtent(bbox=d['bbox'])


@dataclass
class TemporalExtent(Stac):
    interval: list

    def validate(self) -> bool:
        if self.interval is not None:
            return True
        return False

    def _dict(self) -> dict:
        return {'interval': self.interval}

    @staticmethod
    def from_dict(d: dict) -> TemporalExtent:
        return TemporalExtent(interval=d['interval'])


@dataclass
class Extent(Stac):
    spatial: SpatialExtent
    temporal: TemporalExtent

    def validate(self) -> bool:
        if self.spatial is not None and self.spatial.validate() and \
           self.temporal is not None and self.temporal.validate():
            return True
        return False

    def _dict(self) -> dict:
        return {'spatial': self.spatial.dict_data,
                'temporal': self.temporal.dict_data}

    @staticmethod
    def from_dict(d: dict) -> Extent:
        return Extent(
            spatial=SpatialExtent(bbox=d['spatial']['bbox']),
            temporal=TemporalExtent(interval=d['temporal']['interval']))


@dataclass
class Catalog(Stac):
    """Implements Catalog Stac content
    """
    id: str
    title: str
    links: List[Link]
    description: str = None
    stac_extensions: list = field(default_factory=list)
    stac_version: str = VERSION
    type: str = 'Catalog'
    catalog = None

    def _init_from_model(self):
        if self.catalog is None:
            builder = ModelBuilder()
            catalog_class = builder.get_catalog_class()
            self.catalog = \
                catalog_class(type=self.type,
                              stac_version=self.stac_version,
                              stac_extensions=self.stac_extensions,
                              id=self.id,
                              title=self.title,
                              description=self.description,
                              links=[lnk.dict_data for lnk in self.links])
        return self.catalog

    def validate(self) -> bool:
        try:
            self._init_from_model()
            return True
        except Exception:
            return False

    def _dict(self) -> dict:
        return self._init_from_model()

    @staticmethod
    def from_dict(d: dict) -> Catalog:
        return Catalog(id=d['id'], title=d['title'],
                       links=[Link.from_dict(link) for link in d['links']],
                       description=d.get('description'),
                       stac_extensions=d.get('stac_extensions'),
                       stac_version=d.get('stac_version'),
                       type=d.get('type'))


@dataclass
class Collection(Stac):
    id: str
    description: str
    license: str
    extent: Extent
    links: list
    type: str = 'Collection'
    stac_version: str = VERSION
    stac_extensions: list = field(default_factory=list)
    title: str = ''
    keywords: list = field(default_factory=list)
    providers: list = field(default_factory=list)
    summaries: dict = field(default_factory=dict)
    assets: dict = field(default_factory=dict)
    collection = None

    def _init_from_model(self):
        if self.collection is None:
            builder = ModelBuilder()
            collection_class = builder.get_collection_class()

            _pvds = []
            if self.providers is not None:
                _pvds = [pvd.dict_data for pvd in self.providers]

            self.collection = \
                collection_class(type=self.type,
                                 stac_version=self.stac_version,
                                 stac_extensions=self.stac_extensions,
                                 id=self.id,
                                 title=self.title,
                                 description=self.description,
                                 keywords=self.keywords,
                                 license=self.license,
                                 providers=_pvds,
                                 extent=self.extent.dict_data,
                                 summaries=self.summaries,
                                 links=[lnk.dict_data for lnk in self.links],
                                 assets=self.assets)
        return self.collection

    def validate(self) -> bool:
        try:
            self._init_from_model()
            return True
        except Exception:
            return False

    def _dict(self) -> dict:
        return self._init_from_model()

    @staticmethod
    def from_dict(d: dict) -> Collection:
        providers = d.get('providers')
        _providers = None
        if providers is not None and len(providers) > 0:
            _providers = [Provider.from_dict(pvd) for pvd in providers]

        [Provider.from_dict(provider) for provider in d.get('providers')]
        return Collection(id=d['id'], description=d['description'],
                          license=d['license'],
                          extent=Extent.from_dict(d['extent']),
                          links=[Link.from_dict(link) for link in d['links']],
                          type=d.get('type'),
                          stac_version=d.get('stac_version'),
                          title=d.get('title'),
                          stac_extensions=d.get('stac_extensions'),
                          keywords=d.get('keywords'),
                          providers=_providers,
                          summaries=d.get('summaries'),
                          assets=d.get('assets'))


@dataclass
class Item(Stac):
    id: str
    geometry: dict  # shall define geometry class ?
    bbox: list
    properties: dict
    links: list
    assets: dict
    type: str = 'Feature'
    stac_version: str = VERSION
    stac_extensions: list = field(default_factory=list)
    collection: str = None
    item = None

    def _init_from_model(self):
        """ Creates item class from model.
        The model is a bit tricky because it checks the presence of collection
        entry into the link to authorize or not the presence of 'collection'
        property. When no collection can be accepted (link rel=collection no
         present), the model also forbid None collection name, it shall not
        set the parameter.
        :return: The item
        """
        def skip_none(**kwargs):
            ret = {}
            for a, v in kwargs.items():
                if v is not None:
                    ret[a] = v
            return ret

        if self.item is None:
            builder = ModelBuilder()
            item_class = builder.get_item_class()
            self.item = \
                item_class(
                    skip_none(type=self.type,
                              stac_version=self.stac_version,
                              stac_extensions=self.stac_extensions,
                              id=self.id,
                              geometry=self.geometry,
                              bbox=self.bbox,
                              properties=self.properties,
                              links=[lnk.dict_data for lnk in self.links],
                              assets=self.assets,
                              collection=self.collection))
        return self.item

    def validate(self) -> bool:
        try:
            self._init_from_model()
            return True
        except Exception:
            return False

    def _dict(self) -> dict:
        return self._init_from_model()

    @staticmethod
    def from_dict(d: dict) -> Item:
        return Item(id=d['id'],
                    geometry=d['geometry'],
                    bbox=d['bbox'],
                    properties=d['properties'],
                    links=[Link.from_dict(link) for link in d['links']],
                    assets=d['assets'],
                    type=d.get('type'),
                    stac_version=d.get('stac_version'),
                    stac_extensions=d.get('stac_extensions'),
                    collection=d.get('collection'))
