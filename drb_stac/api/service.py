from flask import Flask, Response, abort
from flask_cors import CORS, cross_origin

import os
import json
import logging
import pkg_resources as pkg_res
from datetime import datetime

from utils import replace_all_variable
from drb_stac.stac_model import Catalog, Collection, Item
from drb_stac.layout.stac_layout import StacLayout
from drb_stac import __version__, service_release_date
from pathlib import Path
from waitress import serve
'''
Basic web service implementing dedicated STAC API over produced JSON files.

SOURCE_DATA environment variable is expected to retrieve STAC JSON structure.
STAC_ROOT_URL environment variable is expected to define this service url.

'''
SOURCE_DATA = Path(os.environ.get('SOURCE_DATA', 'gen_output')).absolute()

CATALOG_NAME = StacLayout.default_catalog_name
COLLECTION_NAME = StacLayout.default_collection_name
environment = {
    'STAC_ROOT_URL': os.environ.get('STAC_ROOT_URL',
                                    'http://localhost:5000/stac'),
}


def compute_env(text: str) -> str:
    return replace_all_variable(text, environment)


def _resolve_catalog() -> Catalog:
    with open(os.path.join(SOURCE_DATA, CATALOG_NAME)) as file:
        content = file.read()
    return Catalog.from_dict(json.loads(compute_env(content)))


def _resolve_collection(name) -> Collection:
    with open(os.path.join(SOURCE_DATA, name, COLLECTION_NAME)) as file:
        content = file.read()
    return Collection.from_dict(json.loads(compute_env(content)))


def _resolve_item(col_name, item_name) -> Item:
    with open(os.path.join(SOURCE_DATA, col_name, item_name)) as file:
        content = file.read()
    return Item.from_dict(json.loads(compute_env(content)))


LOGGER = logging.getLogger('STAC_API')

LOGGER.info(f'Starting STAC service from {SOURCE_DATA} location')
LOGGER.info(f'Serving STAC API on URL: {environment["STAC_ROOT_URL"]}')

app = Flask('DRB-PYTHON STAC API DEMONSTRATION')
CORS(app)
app.config["DEBUG"] = True
app.config['CORS_HEADERS'] = 'Content-Type'


@app.route('/stac')
@app.route('/stac/')
@app.route('/stac/<file>')
@cross_origin()
def cat(file=None):
    if file is None or file == 'catalog.json':
        return Response(json.dumps(_resolve_catalog().dict_data),
                        mimetype='application/json')
    else:
        abort(404)


@app.route('/stac/<collection>')
@app.route('/stac/<collection>/')
@app.route('/stac/<collection>/<file>')
@cross_origin()
def col(collection=None, file=None):
    if file is None or file == 'collection.json':
        return Response(json.dumps(_resolve_collection(collection).dict_data),
                        mimetype='application/json')
    else:
        return Response(json.dumps(_resolve_item(collection, file).dict_data),
                        mimetype='application/json')


def _get_about_description(components_list):

    sub_components_descr = {}
    for comp in components_list:
        info = pkg_res.get_distribution(comp).version
        sub_components_descr[comp] = f"v.{info}"

    # date
    cdate = service_release_date
    if service_release_date:
        isodate = service_release_date
        pos = isodate.find('+')
        if pos > -1:
            isodate = service_release_date[:pos]
        date_time_obj = datetime.fromisoformat(isodate)
        cdate = date_time_obj.strftime("%Y-%m-%d")

    about_description = {
        "service version": __version__,
        "API version": "1.0.1",
        "creation_date": cdate,
        "author": "Gael Systems",
        "sub-components": sub_components_descr,
        "description": "this service creates and delivers STAC API catalogs"
    }
    return about_description


@app.route('/stac/about')
def about_description():
    sub_modules_list = ['drb', 'drb_impl_odata']
    return _get_about_description(sub_modules_list)


port = int(os.environ.get('PORT', 5000))
# app.run(debug=True, host='0.0.0.0', port=port)
serve(app, host="0.0.0.0", port=port)
