import re


def _remove_extra_quote(string):
    return string.strip('"').strip("'").strip(' ')


def get_value(name, environment):
    def _replace_var(match):
        var = match.group(1)
        if var in environment:
            return _remove_extra_quote(environment[var])
        return ""

    if environment:
        return re.sub(r'\${(.*?)}', _replace_var, name)
    else:
        return name


def replace_all_variable(text, environment):
    return get_value(text, environment)
