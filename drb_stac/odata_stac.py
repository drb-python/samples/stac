import json
import logging
import os.path

from osgeo import ogr, osr
from datetime import datetime, timezone
from drb import DrbNode
from drb.exceptions import DrbException
from typing import List
from tenacity import retry, stop_after_delay, stop_after_attempt, wait_fixed

"""
    Generation of item from a product DrbNode list (OData like).
    SpatioTemporal Asset Catalog (STAC) Item is a GeoJSON Feature augmented
    with foreign members relevant to a STAC object.
    These include fields that identify the time range and assets of the Item.
    An Item is the core object in a STAC Catalog, containing the core metadata
    that enables any client to search or crawl online catalogs of spatial
    'assets' (e.g., satellite imagery, derived data, DEMs).
    :see: [drb_stac-item structure]
(https://github.com/radiantearth/stac-spec/blob/v1.0.0/item-spec/item-spec.md)
    :see: [ESA CSC OData API](tbc).
"""
logger = logging.getLogger('ODATA-STAC')


def _footprint_to_geo(footprint: str) -> ogr.Geometry:
    """Manages WKT item geography returned by OData API.
    The method parses the input string footprint and convert it to OGR
    feature. When a geometry is defined, it is included as the SRS
    reference of the feature.

    Returns:
    ------
    ogr.Geometry
       The geometry shape representing the item footprint.

    Raises
    ------
    ValueError
       When the footprint string format cannot be parsed.
    """
    if footprint.startswith("geography'SRID="):
        srid, wkt = footprint.split('\'')[1].split(';')
        epsg = int(srid.split("=")[1])
        # srs = osr.SpatialReference()
        # srs.ImportFromEPSG(epsg)
        # geometry = ogr.CreateGeometryFromWkt(wkt, reference=srs)
        geometry = ogr.CreateGeometryFromWkt(wkt)
        return geometry
    else:
        raise ValueError(f"Unknown footprint format: {footprint}")


def get_attribute(node: DrbNode, name: str, error: bool = True, default=None):
    """Manage node attribute retrieval.
    Retrieval can raise an error (default) or not when boolean *error*
    attribute is set.
    """
    try:
        value = node.get_attribute(name, node.namespace_uri)
        if value is None:
            raise ValueError(f"Attribute {name} is empty.")
        return value
    except Exception as excp:
        msg = f"Cannot retrieve node attribute {name} in {node.path.name}"
        if error:
            raise ValueError(msg) from excp
        else:
            logger.warning(msg + ": " + str(excp))
            return default


def item_content_from_odata_nodes(products: List[DrbNode]):
    """
    Creates product info part of the drb_stac item extracted from a
    Product/Attribute OData entity. Others info related to the Stac structure
    is built in the Stac model.

    :param products: list of ODataNodes containing attributes.

    :return: an iterable generator containing the list of drb_stac item
       information:

       Mandatory :
       ----------
       + id
       + geometry
       + bbox
       + properties/created
       + properties/updated
       + properties/datetime/start_datetime
       + properties/datetime/stop_datetime

       Optionnals:
       ----------
       - properties/title
       - properties/description
       - properties/eo:cloud_cover
       - properties/eo:orbit
       - properties/
    """
    for product in products:
        yield item_content_from_odata_node(product)


@retry(stop=(stop_after_delay(120) | stop_after_attempt(5)),
       wait=wait_fixed(15))
def item_content_from_odata_node(product: DrbNode):
    stac_item = dict()
    stac_item['id'] = product.name

    try:
        geom = _footprint_to_geo(get_attribute(product, 'Footprint'))
        stac_item['geometry'] = json.loads(geom.ExportToJson())
        stac_item['bbox'] = [x for x in geom.GetEnvelope()]
    except Exception as excp:
        raise ValueError(
            f"Cannot compute product position for {product.name}") \
            from excp

    properties = dict()
    properties['uuid'] = get_attribute(product, 'Id')
    properties['created'] = get_attribute(product, 'PublicationDate')
    properties['updated'] = get_attribute(product, 'ModificationDate')
    # properties['created'] = datetime.now(timezone.utc).isoformat()
    # properties['updated'] = properties['created']
    datetime_begin_end = get_attribute(product, 'ContentDate', error=False)
    if datetime_begin_end:
        properties['start_datetime'] = datetime_begin_end['Start']
        properties['end_datetime'] = datetime_begin_end['End']
    else:
        raise ValueError(
            f"No relevant date for product {product.name}")

    properties['datetime'] = datetime_begin_end['Start']

    try:
        properties['file:size'] = get_attribute(product, 'ContentLength')
    except Exception:
        logger.warning("Product size not found")

    try:
        attributes = product['Attributes']
    except Exception:
        logger.error(f'Cannot retrieve Attribute from {product.path.name}')
        stac_item['properties'] = properties
        return stac_item

    properties['title'] = os.path.splitext(product.name)[0]
    try:
        title = attributes['resourceTitle']
        description = attributes['resourceAbstract']
        properties['description'] =\
            f'<b>{title.value}</b>:<br/>{description.value}'
    except (IndexError, DrbException, KeyError):
        logger.warning(f'Description not found in {product.path.name}')

    try:
        cloud_cover = attributes['cloudCover']
        properties['eo:cloud_cover'] = float(cloud_cover.value)
    except (IndexError, DrbException, KeyError):
        logger.warning(f'Cloud cover not found in {product.path.name}')

    try:
        orbit = attributes['orbitNumber']
        properties['eo:orbit'] = int(orbit.value)
    except (IndexError, DrbException, KeyError):
        logger.warning(f'Orbit not found in {product.path.name}')

    stac_item['properties'] = properties

    assets = {
        "metadata_json": {
            "title": "Online product metadata",
            "href": product.path.name + "/Attributes?$format=json",
            "roles": ["metadata"],
            "type": "application/json"
        },
        "metadata_xml": {
            "title": "Online product metadata",
            "href": product.path.name + "/Attributes?$format=xml",
            "roles": ["metadata"],
            "type": "application/xml"
        }


    }

    content_type = get_attribute(product, 'ContentType', error=False,
                                 default='application/octet-stream')

    online = get_attribute(product, 'Online', error=False, default=True)

    if product.name.startswith('S2'):
        assets["thumbnail"] = {
            "title": "Quick Look",
            "href":
                product.path.name + "/AttachedFiles('quick-look.jpg')/$value",
            "roles": ["thumbnail"],
            "type": "image/jpeg"
        }

    if online:
        assets["download"] = {
            "title": "Download access",
            "href": product.path.name + "/$value",
            "roles": ["data"],
            "type": content_type
        }
    stac_item['assets'] = assets
    return stac_item
