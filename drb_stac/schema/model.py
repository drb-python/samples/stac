import json

import warlock
import importlib.resources as rs
import resources
from resources import schema
import os


class ModelBuilder:
    """Builds Stac data model
    """
    def __init__(self):
        with rs.path(resources.schema, 'catalog.json') as file:
            self.catalog_json_file = file

        with rs.path(resources.schema, 'collection.json') as file:
            self.collection_json_file = file

        with rs.path(resources.schema, 'item.json') as file:
            self.item_json_file = file

        if not os.path.isfile(self.catalog_json_file):
            raise FileNotFoundError(
                f"Cannot find schema file {self.catalog_json_file}")

        if not os.path.isfile(self.collection_json_file):
            raise FileNotFoundError(
                f"Cannot find schema file {self.collection_json_file}")

        if not os.path.isfile(self.item_json_file):
            raise FileNotFoundError(
               f"Cannot find schema file {self.item_json_file}")

        self.catalog_class = None
        self.collection_class = None
        self.item_class = None

    def get_catalog_class(self):
        if not self.catalog_class:
            with open(self.catalog_json_file) as file:
                schema = json.load(file)
                self.catalog_class = warlock.model_factory(schema)
        return self.catalog_class

    def get_collection_class(self):
        if not self.collection_class:
            with open(self.collection_json_file) as file:
                schema = json.load(file)
                self.collection_class = warlock.model_factory(schema)
        return self.collection_class

    def get_item_class(self):
        if not self.item_class:
            with open(self.item_json_file) as file:
                schema = json.load(file)
                self.item_class = warlock.model_factory(schema)
        return self.item_class
