
from . import _version
__version__ = _version.get_versions()['version']
service_release_date = _version.get_versions()['date']
