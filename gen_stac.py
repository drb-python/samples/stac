import datetime
import logging
import importlib.resources as pkg
import json
import os
import shutil
import sys
from typing import Union
import pkg_resources

from drb import DrbNode

import tests.data as data
from drb_stac import odata_stac, __version__
from drb_stac.stac_model import Catalog, Collection, Item
from drb_stac.stac_model import Extent, SpatialExtent, TemporalExtent
from drb_stac.layout.stac_layout import StacLayout, Layout
from drb.abstract_node import Predicate
from drb_impl_odata.odata_services_nodes import ODataServiceNodeCSC
from drb_impl_odata import ODataQueryPredicate
from drb_impl_http.oauth2.HTTPOAuth2 import HTTPOAuth2
from requests.auth import HTTPBasicAuth

logging.basicConfig(stream=sys.stderr, level=logging.INFO)
logger = logging.getLogger("STAC-GEN")

# OData service settings
SERVICE = os.environ.get(
    'gss_service',
    'https://demo1.databridge.gael.fr/gss-catalogue')
USERNAME = os.environ.get('gss_username')
PASSWORD = os.environ.get('gss_password')
OAUTH_TOKEN_SERVICE = os.environ.get('auth_token_service')
OAUTH_CLIENT_ID = os.environ.get('auth_client_id')
OAUTH_CLIENT_SECRET = os.environ.get('auth_client_secret')

description = pkg.read_text(data, "sentinel_description.txt")

stac_extensions = [
    'https://stac-extensions.github.io/eo/v1.0.0/schema.json',
    'https://stac-extensions.github.io/projection/v1.0.0/schema.json',
    'https://stac-extensions.github.io/view/v1.0.0/schema.json',
    'https://stac-extensions.github.io/file/v2.1.0/schema.json',
    'https://stac-extensions.github.io/sar/v1.0.0/schema.json'
]

# CATALOGUE
catalog_id = 'DEMO_CATALOG_V1'
catalog_title = 'DRB-PYTHON STAC DEMO CATALOG '
catalog_date = datetime.datetime.now().strftime("%a %b %d %H:%M:%S %Y")


# catalogue description and versions
vesion_DRB = pkg_resources.get_distribution("drb").version
vesion_DRB_Odata = pkg_resources.get_distribution("drb-impl-odata").version
catalog_description = description + f'\nGael Systems - ' + \
                    f'Created {catalog_date}\n\n' + \
                    f'Version: {__version__}' + \
                    f' based on DRB v.{vesion_DRB}' + \
                    f' and driver DRB Odata v.{vesion_DRB_Odata}.'


try:
    top_default = int(os.environ.get('gss_top_default', '10000'))
    catalogue5p_nblast = int(os.environ.get('catalogue5p_nlast', '100'))
except Exception as e:
    top_default = 10000
    catalogue5p_nblast = 100

_COLLECTIONS = [
    {'id': 'SENTINEL_1',
     'title': 'Sentinel-1',
     'filter': "startswith(Name, 'S1')",
     'top': top_default,
     'description': "Sample S1 STAC Collection from Odata "
                    "service by queried by name "
                    "```\n "
                    "$filter=startswith(Name, 'S1')"
                    "```",
     'spatial': SpatialExtent(bbox=[[-180.0, -90.0, 180.0, 90.0]]),
     'summary_content': "s1_summary.json",
     'keywords_content': "s1_keyword.json"
     },
    {'id': 'SENTINEL_2',
     'title': 'Sentinel-2',
     'filter': "startswith(Name, 'S2')",
     'top': top_default,
     'description': "Sample S2 STAC Collection from Odata "
                    "service by queried by name "
                    "```\n "
                    "$filter=startswith(Name, 'S2')"
                    "```",
     'spatial': SpatialExtent(bbox=[[-180.0, -90.0, 180.0, 90.0]]),
     'summary_content': "s2_summary.json",
     'keywords_content': "s2_keyword.json"
     },
    {'id': 'SENTINEL_3',
     'title': 'Sentinel-3',
     'filter': "startswith(Name, 'S3')",
     'top': top_default,
     'description': "Sample S3 STAC Collection from Odata "
                    "service by queried by name "
                    "```\n "
                    "$filter=startswith(Name, 'S3')"
                    "```",
     'spatial': SpatialExtent(bbox=[[-180.0, -90.0, 180.0, 90.0]]),
     'summary_content': "s3_summary.json",
     'keywords_content': "s3_keyword.json"
     },
    {'id': 'SENTINEL_5',
     'title': f'Sentinel-5P: {catalogue5p_nblast} last inserted product',
     'filter': "startswith(Name, 'S5P')",
     'top': catalogue5p_nblast,
     'description': "Sample S3 STAC Collection from Odata "
                    "service by queried by name "
                    "```\n "
                    "$filter=startswith(Name, 'S5P')"
                    "```",
     'spatial': SpatialExtent(bbox=[[-180.0, -90.0, 180.0, 90.0]]),
     'summary_content': "s5p_summary.json",
     'keywords_content': "s5p_keyword.json"
     },
    {'id': 'SENTINEL_2_BRETAGNE',
     'title': 'Sentinel-2 collection by ROI',
     'filter': "startswith(Name,'S2') and "
               "OData.CSC.Intersects(area=geography%27SRID=4326;POLYGON(("
               "-2.9396525469094588 47.73283920337374,"
               "-2.9001931876733265 47.73283920337374,"
               "-2.9001931876733265 47.74770130750534,"
               "-2.9396525469094588 47.74770130750534,"
               "-2.9396525469094588 47.73283920337374))%27)",
     'top': top_default,
     'description': "Sample S2 STAC Collection over ROI with the filter: \n"
                    "```\n"
                    "$filter=startswith(Name, 'S2') and\n"
                    "  OData.CSC.Intersects(location=Footprint,"
                    "area=geography’SRID=4326;\n"
                    "POLYGON((-2.9396525469094588 47.73283920337374,\n"
                    "         -2.9001931876733265 47.73283920337374,\n"
                    "         -2.9001931876733265 47.74770130750534,\n"
                    "         -2.9396525469094588 47.74770130750534,\n"
                    "         -2.9396525469094588 47.73283920337374))')\n"
                    "```",
     'spatial': SpatialExtent(bbox=[[-2.94, 47.73, -2.90, 47.75]]),
     'summary_content': "s2_summary.json",
     'keywords_content': "s2_keyword.json"
     },
    {'id': 'SENTINEL_2_BY_CLOUD_COVER',
     'title': 'Sentinel-2 collection with less than 80% of cloud cover.',
     'filter': "startswith(Name, 'S2') and "
               "Attributes/OData.CSC.DoubleAttribute/any("
               "att:att/Name eq 'cloudCover' and "
               "att/OData.CSC.DoubleAttribute/Value lt 80)",
     'top': top_default,
     'description': "Sample S2 STAC Collection of product "
                    "with less than 80% of cloud cover."
                    "The filter: <br/>"
                    "```\n "
                    "$filter=startswith(Name, 'S2') and\n"
                    " Attributes/OData.CSC.DoubleAttribute/any(\n "
                    " att:att/Name eq 'cloudCover' and \n "
                    " att/OData.CSC.DoubleAttribute/Value lt 80)\n"
                    "```",
     'spatial': SpatialExtent(bbox=[[-180.0, -90.0, 180.0, 90.0]]),
     'date_start': -2,
     'date_end': 'NOW',
     'summary_content': "s2_summary.json",
     'keywords_content': "s2_keyword.json"
     }
]


def get_odata_node_from_predicate(
        predicate: Union[Predicate, str, int, slice, tuple]) -> DrbNode:
    if OAUTH_TOKEN_SERVICE is not None:
        auth = HTTPOAuth2(USERNAME, PASSWORD, OAUTH_TOKEN_SERVICE,
                          OAUTH_CLIENT_ID, OAUTH_CLIENT_SECRET)
    else:
        auth = HTTPBasicAuth(USERNAME, PASSWORD)

    svc = ODataServiceNodeCSC(SERVICE, auth)
    return svc[predicate]


def gen_collection(nodes, mgr: StacLayout, collection: Collection,
                   collection_layout: Layout, output_col: str,
                   top: int):
    nb = len(nodes)
    len_nodes = min(nb, top)

    min_date = datetime.datetime.now()
    max_date = datetime.datetime(2000, 1, 1)

    for index_node in range(len_nodes):
        node = nodes[index_node]
        logger.info(f'Managing {node.name}')
        try:
            item = odata_stac.item_content_from_odata_node(node)
        except Exception as e:
            logger.error(f"Item Extraction raise error({str(e)}), "
                         f"ingestion of {node.name} cancelled")
            continue
        title = item.get('properties').get('title') \
            if item.get('properties') is not None else None
        # search for the min and max date for all products
        if item.get('properties') is not None:
            product_date_str = item.get('properties').get('datetime')
            pos = product_date_str.rfind(".")
            if pos > 0:
                product_date = datetime.datetime.fromisoformat(
                    product_date_str[:pos])
                if product_date < min_date:
                    min_date = product_date
                if product_date > max_date:
                    max_date = product_date
        item_layout = mgr.item_layout(
            item, title=title, parent_collection=collection)
        stac_item = Item(id=item['id'],
                         geometry=item['geometry'],
                         bbox=item['bbox'],
                         properties=item['properties'],
                         links=item_layout.get_links(),
                         stac_extensions=stac_extensions,
                         assets=item['assets'],
                         collection=collection['id'])

        try:
            item_layout.write(stac_item, working_dir=output_col)
        except Exception as e:
            logger.error(f"Item layout error({str(e)}), "
                         f"ingestion of {node.name} cancelled")
            continue

        item_title = f"{os.path.splitext(item['id'])[0]}"
        collection_layout.add_item(href=item_layout.self.href,
                                   link_type=item_layout.self.type,
                                   title=item_title)

    logger.info(f'End manage {len_nodes} nodes')

    # update min and max date for the collection
    t1 = min_date.strftime("%Y-%m-%dT%H:%M:%S.000Z")
    t2 = max_date.strftime("%Y-%m-%dT%H:%M:%S.000Z")
    collection.extent.temporal = TemporalExtent(
                                        interval=[[t1, t2]])
    collection['extent']['temporal'] = collection.extent.temporal.dict_data

    # Collection Dict shall be updated after item updates.
    collection['links'] = StacLayout.dict_data(collection_layout.get_links())
    collection_layout.write(collection, working_dir=output_col)


def gen_stac():
    layout_mgr = StacLayout(
        root_href="${STAC_ROOT_URL}",
        root_title="STAC repository")

    collections = []

    output_stac = os.environ.get("SOURCE_DATA", 'gen_output')
    output = os.environ.get("SOURCE_DATA_WORK", 'gen_output_work')
    output_tmp = os.environ.get("SOURCE_DATA_TMP", 'gen_output_tmp')

    if not os.path.isdir(output):
        os.mkdir(output)

    for definition_collection in _COLLECTIONS:
        col_layout = \
            layout_mgr.collection_layout(
                parent_title=catalog_title,
                identifier=definition_collection['id'],
                title=definition_collection['title'])

        filter = definition_collection['filter']

        date_end = None
        if 'date_end' in definition_collection.keys():
            date_end = definition_collection['date_end']
        if date_end is not None:
            if date_end == 'NOW':
                date_end = datetime.datetime.now().strftime(
                    '%Y-%m-%dT%H:%M:%S.000Z')
            else:
                if len(filter) > 0:
                    filter += " and "
                filter += f'PublicationDate lt {date_end}'

        date_start = None
        if 'date_start' in definition_collection.keys():
            date_start = definition_collection['date_start']
        if date_start is None:
            date_start = "2015-06-23T01:52:00.000Z"
        else:
            if isinstance(date_start, int) and date_end is not None:
                dd_end = datetime.datetime.strptime(date_end[:19],
                                                    '%Y-%m-%dT%H:%M:%S')
                dd_start = dd_end + datetime.timedelta(days=date_start)
                date_start = dd_start.strftime('%Y-%m-%dT%H:%M:%S.000Z')
            if len(filter) > 0:
                filter += " and "
            filter += f'PublicationDate gt {date_start}'

        collection_predicate = ODataQueryPredicate(
            filter=filter,
            top=definition_collection['top'])

        temporal = TemporalExtent(
            interval=[[date_start,
                       date_end]])

        collect = Collection(
            id=definition_collection['id'],
            description=definition_collection['description'],
            license="proprietary",
            extent=Extent(definition_collection['spatial'],
                          temporal),
            links=col_layout.get_links(),
            stac_extensions=stac_extensions,
            title=definition_collection['title'],
            keywords=json.loads(pkg.read_text(
                data, definition_collection['keywords_content'])),
            providers=[],  # Not implemented (dict can be used)
            summaries=json.loads(pkg.read_text(
                data, definition_collection['summary_content'])),
            assets={}
        )
        collections.append(collect)

        logger.info(f"\nCollection : {definition_collection['title']}\n")

        gen_collection(nodes=get_odata_node_from_predicate(
            collection_predicate),
            mgr=layout_mgr,
            collection=collect,
            collection_layout=col_layout,
            output_col=output,
            top=definition_collection['top'])

        title_cat = definition_collection['title']
        logger.info(f'\n\nEnd manage catalog {title_cat}')

    catalog_layout = layout_mgr.catalog_layout(
        title=catalog_title, collections=collections)
    stac_catalog = Catalog(id=catalog_id, title=catalog_title,
                           links=catalog_layout.get_links(),
                           description=catalog_description)

    catalog_layout.write(stac_catalog, working_dir=output)

    if os.path.isdir(output_stac):
        # os.rename(output_stac, output_tmp)
        shutil.move(output_stac, output_tmp)

    shutil.move(output, output_stac)

    if os.path.isdir(output_tmp):
        shutil.rmtree(output_tmp)

    # Now browse the StacLayout path...
