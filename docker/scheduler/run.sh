# Docker container shall be run with the following command line:
# run script has 3 arguments:
#   run.sh <gss service url> <gss service login> <gss service password>

if [ $# -ne 3 ]
then
   echo "Missing image parameters:" 1>&2
   echo "   run.sh <gss service url> <gss service login> <gss service password>" 1>&2
   exit 1
fi
gss_service=${1}
gss_username=${2}
gss_password=${3}

# WARN OAuth2.0 not configured for demo1 at gael.fr
# to use basic auth do not defines any auth service:
#auth_token_service=https://auth.databridge.gael-systems.com/auth/realms/dbs/protocol/openid-connect/token
#auth_client_id=gss-catalogue

docker run --env gss_username=${gss_username} \
           --env gss_passowrd=${gss_password} \
           --env gss_service=${gss_service} \
           stac_scheduler
